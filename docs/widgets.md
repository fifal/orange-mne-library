# Widgets
In this chapter, available widgets will be briefly described.

## Data IO
Widgets from this category are utilized to load input files, or save the results.

---
### BrainVision EEG Reader
<!--![BrainVision reader icon](images/brainvision/brainvision.png)-->

This widget is used to load `.vhdr` files recorded from the BrainVision.

![BrainVision reader icon](images/brainvision/brainvision.png)

The widget window contains a clickable button, which opens the file selection dialog and a combo box, which
allows to set the montage type to visualize various plots.
After the file is selected, or the montage is changed, the loaded data are automatically sent to the output port of the widget.

**Input**
- BrainVision's `.vhdr` file

**Output**
- MNE-Python's object with type of `mne.io.Raw`

---

### EEGLAB Reader
![EEGLAB set files reader](images/eeglab/widget.png)

This widget is used to load `.set` files from EEGLAB.

![EEGLAB widget window](images/eeglab/eeglab-widget-window.png)

The widget window contains a clickable button, which opens the file selection dialog and a combo box, which
allows to set the montage type to visualize various plots.
After the file is selected, or the montage is changed, the loaded data are automatically sent to the output port of the widget.

**Input**
- EEGLAB's `.set` file

**Output**
- MNE-Python's object with type of `mne.io.Raw`


---

### Matlab File Reader
![Matlab File reader icon](images/mat/icon.png)

This widget is utilized to load data files in a Matlab format (`.mat`).

![Matlab File reader widget](images/mat/widget-window.png)

The widget window contains a clickable button, which opens the file selection dialog.
After the file is selected the loaded data are automatically sent to the output port of the widget.

**Input**
- Matlab's `.mat` file

**Output**
- `dict`

---

### Fif File Save
![File Save icon](images/fif/icon-save.png)

It is possible to save three different types of processed data – raw, epochs or evokeds.

![File Save widget](images/fif/widget-window-save.png)

If the processing takes a long time, or to load the data in another workflow, 
it may be useful to save the processed data into a file first and load them later. 
The Fif File Save widget has this functionality. 

Each type of processed data has its suffix after the file is saved. The suffix is important because when loading the data 
using the MNE library, each type requires a different method:

Raw: `-raw.fif`

Epochs: `-epo.fif`

Evokeds: `-ave.fif`

**Input**
- MNE-Python's object with type of `mne.io.Raw`, `mne.Epochs`, or `mne.Evoked`

**Output**
- Fif File

---

### Fif Reader 
![File Load icon](images/fif/icon.png)

The Fif Reader widget is used to load data files that were saved by the [Fif File Save widget](#fif-file-save).

![File Load widget](images/fif/widget-window.png)

**Input**
- Fif File 

**Output**
- MNE-Python's object with type of `mne.io.Raw`, `mne.Epochs`, or `mne.Evoked`

---

---

## Preprocessing
Widgets in this category are utilized to prepare the raw data for processing.Such widgets include epoch extraction, channel selection, filtering, and more.

---

### Channel Select
![Channel Select icon](images/channel-select/icon.png)

The raw recorded electrophysiological data may contain several channels, but for their processing, 
researchers may need only a few of them. For this purpose, a Channel Select widget is present 
in the library. The Channel Select allows researchers to select specific channels from the raw data.

![Channel Select widget](images/channel-select/widget-window.png)

If data were sent to the widget, the widget will display all found channels. The user then can select desired channels only
by writing their names comma separated in a line edit.

**Input**
- MNE-Python's object with type of `mne.io.Raw`

**Output**
- MNE-Python's object with type of `mne.io.Raw`

---

### Epoch Extraction
![Epoch Extraction icon](images/epoch-extraction/icon.png)

The electrophysiological signal recordings can be very long. However, in some cases, 
the researchers are interested only in part of the data, where particular stimuli have occurred. 
For such cases, epoch extraction methods are available.

![Epoch Extraction widget](images/epoch-extraction/widget-window.png)

Parameter `tMin` corresponds to Pre-stimulus time to extract. Parameter `tMax` corresponds to Post-stimulus time to extract.

If data were already loaded and annotations were found a block with checkboxes will be displayed. 
In this block, users can select particular stimuli for the extraction.

**Input**
- MNE-Python's object with type of `mne.io.Raw`

**Output**
- MNE-Python's object with type of `mne.Epochs`

---

### Filter
![Filter icon](images/filter/icon.png)

Filtering is an essential step of preprocessing, as recorded signals can contain a lot of signal noise. 
For example, electrophysiological recordings can be noisy because of power line interference. 

The filter widget supports two methods of filtering – IIR or FIR. The graphical user interface is dynamically changing based on the selected method. 

![Filter IIR method](images/filter/iir-widget-window.png)

The essential parameters of the IIR method are lower and upper cutoff frequency. Additionally, there are several 
advanced parameters, such as type and order of the filter and their specifying parameters. 
The types of the filter can be Chebyshev, Butterworth, Elliptic, or Bessel/Thompson.

![Filter FIR method](images/filter/fir-widget-window.png)

Similarly, like the IIR method, the FIR method offers the setting of essential parameters – lower and upper 
pass-band edge. The specific parameter settings for the FIR method are filter length, FIR window type, and phase.

**Input**
- MNE-Python's object with type of `mne.io.Raw`, `mne.Epochs`, or `mne.Evoked`

**Output**
- MNE-Python's object with type of `mne.io.Raw`, `mne.Epochs`, or `mne.Evoked`

---

### Baseline Correction
![Baseline Correction icon](images/baseline-correction/icon.png)

Baseline correction is a significant part of preprocessing. An uneven amplitude shifts may occur in the recorded signal. 
For further processing and analysis, it is necessary to compensate for such amplitude shifts.

![Baseline Correction widget](images/baseline-correction/widget-window.png)

The widget has two text inputs that are utilized to define an interval from the whole epoch length, 
which will be used for the baseline correction.

**Input**
- MNE-Python's object with type of `mne.Epochs`

**Output**
- MNE-Python's object with type of `mne.Epochs`

---

### Artifact Rejection
![Artifact Rejection icon](images/rejection/icon.png)

Individual epochs extracted from the recorded signal may contain artifacts, which may bias the results. 
Such artifacts may be caused by, for example, eye movement (eyewink), or head movement. 
The simplest method to detect such artifacts is by the amplitude of the signal.

![Artifact Rejection window](images/rejection/widget-window.png)

After the threshold is set, the widget iterates over epochs and finds an amplitude peak for each. 
If the absolute value of the peak is greater than the threshold, the epoch is rejected.

**Input**
- MNE-Python's object with type of `mne.Epochs`

**Output**
- MNE-Python's object with type of `mne.Epochs`

---

### Averaging
![Averaging icon](images/averaging/icon.png)

Electrophysiological data may contain artifacts even after filtering, to get a better picture of the final signal's final form, it is useful to take advantage of the signal averaging methods. 
For this purpose, there is an EEG Averaging widget available in the created library.

![Averaging widget](images/averaging/widget-window.png)

 When the widget receives the data, it shows a group of checkboxes, where each checkbox represents a stimulus category. Apart from the name of the stimulus, a count of stimuli is displayed. 
 Users can select specific stimulus over which will be the signal from epochs averaged. Although it is usually useful to average signals that belong to one particular stimulus, users have the possibility to average signals over more than one stimulus.

**Input**
- MNE-Python's object with type of `mne.Epochs`

**Output**
- MNE-Python's object with type of `mne.Evoked`

---

### Concatenation
![Concatenation icon](images/concat/icon.png)

![Concatenation widget](images/concat/widget-window.png)

Orange allows for multiple data inputs through one signal line into a widget;
however, it does not allow to output several data utilizing one signal line out
of a widget. This functionality may be a limitation for researchers that have
multiple input files. The Concatenation widget is included in the library to
get around this limitation. 

To concatenate multiple data inputs correctly all inputs must have the **same number of channels**.

**Input**
- MNE-Python's object with type of `mne.io.Raw`, `mne.Epochs`

**Output**
- MNE-Python's object with type of `mne.io.Raw`, `mne.Epochs`

---

### Re-sample
![Resample icon](images/resample/icon.png)

Resample widget can be used to resample extracted epochs to required sampling frequency.

![Resample widget](images/resample/widget-window.png)

The Resample widget may resample the sampling frequency of the extracted epochs to a range of 1–1000 Hz.
This may be usefull for workflows where high resolution of the signal is not necessary and would slow down the processing of the data.

**Input**
- MNE-Python's object with type of `mne.Epochs` 

**Output**
- MNE-Python's object with type of `mne.Epochs` 

---

### Grand Average
![Grand Average icon](images/grand-average/icon.png)

The grand average is an average calculated from multiple averages.

![Grand Average widget](images/grand-average/widget-window.png)

This can be beneficial, for example, when examining the data from several testing subjects, as this approach can mitigate some abnormalities or artifacts.

**Input**
- Multiple MNE-Python's objects with type of `mne.Evoked`

**Output**
- MNE-Python's object with type of `mne.Evoked`

---

---

## Feature Extraction

---

### Peak Latency
![Peak Latency icon](images/peak/icon.png)

![Peak Latency widget](images/peak/widget-window.png)

There are two text inputs for the time interval and a combo box for the mode selection. 
The time interval is useful if we want to find a peak in a specific range of an epoch. 
There are three modes of peak finding available – *Positive*, *Negative*, and *Absolute*. 
The *Positive* mode will find the peak in the positive values, the *Negative* mode is the exact opposite, and it will find 
the peak in the negative values. The *Absolute* mode will find the most significant peak across positive and negative values. 
Apart from the peak latency, the widget also shows the peak amplitude.

**Input**
- MNE-Python's object with type of `mne.Evoked`

**Output**
- None

---

### Epochs to Vector
![Epochs to Vector icon](images/epochs-to-vector/icon.png)

![Epochs to Vector widget](images/epochs-to-vector/gui.png)

Epochs to Vector is a widget that converts `mne.Epochs` into a vector which may be utilized in classification workflows.
The graphical user interface of the widget is quite minimalistic as it does not have any interactive inputs. 
The widget contains one label with information about how many epochs were converted to a vector in total.

**Input**
- MNE-Python's object with type of `mne.Epochs`

**Output**
- `dict` with the following format:
    - ```
      {
            data: <vector_with_the_data>, 
            sfreq: <sampling_frequency>,
            stimuli: <selected_stimuli>,
            channels: <channel_names>
      }
      ```
    
---

---

## Classification

---

### Prepare Vectors 
![Prepare Vectors icon](images/prep-vec/icon.png)

![Prepare Vectors widget](images/prep-vec/widget-window.png)

Widget expects two input vectors – target and non-target and creates the Classification struct from
them, which is required for the classification process

**Input**
- `dict` – Target vector
- `dict` – Non-target vector 

**Output**
- `EegClassification.structs.ClassificationStruct`

---

### Create Classification Struct 
![Create Classification Struct icon](images/create-class/icon.png)

![Create Classification Struct widget](images/create-class/widget-window.png)

The Create Classification Struct widget, similarly, like the Prepare Vectors
widget creates a Classification struct from the input vector; however, the
classification classes of the vector are based on the stimulus selected in the
epoch extraction step.

**Input**
- `dict` – Input vector 

**Output**
- `EegClassification.structs.ClassificationStruct`

---

### Concatenate Classification Structs 
![Concatenate Classification Structs icon](images/concat-class/icon.png)

![Concatenate Classification Structs widget](images/concat-class/widget-window.png)

The Concatenate Classification Structs widget, as the name suggests, is
useful to concatenate multiple Classification structs together.  On top of
that, it crops all vectors size to match vector with the minimum size to
prevent overfitting, for example, when one stimulus has 400 events, and the
other has only 20, the classifier would be overfitted on the first stimulus.

**Input**
- multiple `dict` – Input vectors 

**Output**
- `EegClassification.structs.ClassificationStruct`

---

### Windowed Means 
![Windowed Means icon](images/windowed-means/icon.png)

![Windowed Means widget](images/windowed-means/widget-window.png)

The Windowed Means widget extracts features in the following way. It splits
the extracted epochs into intervals based on the minimal latency, maximum
latency, and the number of steps. It then calculates the average value for
each EEG channel for each range.

Parameters:
- `Min. latency`: Lower border for the intervals (after the stimulus)
- `Max. latency`: Upper border for the intervals (after the stimulus)
- `Number of steps`: Number of intervals on selected range (lower, upper)
- `Pre-epoch`: Sets how much time was extracted pre-stimulus in epochs
- `Sampling frequency`: Sets the sampling frequency of the signal

**Input**
- `EegClassification.structs.ClassificationStruct`

**Output**
- `EegClassification.structs.ClassificationStruct`

---

### Reject Amplitude 
![Reject Amplitude icon](images/rejection/icon-vector.png)

![Reject Amplitude widget](images/rejection/widget-window-vector.png)

Identically as the [Artifact Rejection](#artifact-rejection), the Reject Amplitude widget rejects
epochs whose amplitude exceeds the threshold limit. The difference is that
this widget works with vectors opposed to the Artifact Rejection, which
works with MNE’s library objects.

Additionally, this widget has a switch between units (V and μV)

**Input**
- `EegClassification.structs.ClassificationStruct`

**Output**
- `EegClassification.structs.ClassificationStruct`

---

### CNN Reshape 
![CNN Reshape icon](images/cnn-reshape/icon.png)

![CNN Reshape widget](images/cnn-reshape/widget-window.png)

This widget adds a singleton dimension to enable CNN classification using the Convolutional Neural Network widget.

>Note: This widget is specific to one experiment ([Evaluation of the convolutional neural networks experiment](https://www.sciencedirect.com/science/article/abs/pii/S1746809419304185))

**Input**
- `EegClassification.structs.ClassificationStruct`

**Output**
- `EegClassification.structs.ClassificationStruct`

---

### Train Test Split
![Train Test Split icon](images/test-train/icon.png)

![Train Test widget](images/test-train/widget-window.png)

It is necessary to provide training and testing dataset to use classification methods that utilize supervised learning. 
To get such datasets it is possible to split one extensive dataset into two parts – training and testing. The Train Test Split widget offers this functionality.

The Train Test Split widget contains one input for numbers and one button. 

Using the input for numbers, researchers can easily set the percentage that represents the proportion of the dataset to include in the test split.

**Input**
- `EegClassification.structs.ClassificationStruct`

**Output**
- `EegClassification.structs.TestTrainStruct`

---

### Train Test Select
![Train Test Select icon](images/test-train/icon-select.png)

![Train Test widget](images/test-train/widget-window-select.png)

Similarly, like the [Train Test Split](#train-test-split), this widget is utilized to prepare the data for the classification. 
The only difference is that this widget requires two input datasets instead of one – training and testing, 
widget then converts datasets into a structure that necessary in classification widgets.

**Input**
- `EegClassification.structs.ClassificationStruct`

**Output**
- `EegClassification.structs.TestTrainStruct`

---

### Neighbor Average
![Neighbor Average icon](images/neighbor-average/icon.png)

![Neighbor Average widget](images/neighbor-average/widget-window.png)

The Neighbor Average widget is similar to the [Windowed Means](#windowed-means) widget, except instead of intervals, 
it averages N number of trials in epochs together.

The widget contains two checkboxes to select which datasets will be averaged.

**Input**
- `EegClassification.structs.TestTrainStruct`

**Output**
- `EegClassification.structs.TestTrainStruct`

---

### Linear Discriminant Analysis
![Linear Discriminant Analysis icon](images/lda/icon.png)

![Linear Discriminant Analysis icon](images/lda/widget-window.png)

The LDA classification from the scikit library is available through the Linear Discriminant Analysis widget.

The results include four metrics for both validation and test data –
Accuracy, AUC (Area under the ROC Curve), Precision, and Recall. Additionally, the results include a standard deviation as well.

>Note: If only one result is displayed it correspons to **Accuracy**

**Input**
- `EegClassification.structs.TestTrainStruct`

**Output**
- None

---

### Convolutional Neural Network
![Convolutional Neural Network icon](images/cnn/icon.png)

A simple widget that uses Keras and Tensorflow
libraries

![Convolutional Neural Network icon](images/cnn/widget-window.png)

The implemented model of CNN is specific for the experiment described in original [article](https://www.sciencedirect.com/science/article/abs/pii/S1746809419304185). It would be possible
to let the researchers specify the model details; however, the graphical user interface would be highly complex as 
the models have numerous different settings. For this reason, only the model from the original experiment was 
implemented as a proof of concept.

**Input**
- `EegClassification.structs.TestTrainStruct`

**Output**
- None

---

---

## Visualization
Widgets in this category are usefull for the visualization of the raw or processed data.

---

### EEG Plot
![Simple plot widget](images/simple-plot/widget.png)

EEG plot widget utilizes the `plot()` function from the MNE library.

![Simple plot window](images/simple-plot/widget-window.png)

The widget contains a clickable button, which opens the generated plot for the data.

**Input**
- MNE-Python's object types: `mne.io.Raw`, `mne.Epochs`, or `mne.Evoked`

**Output**
- None

---

### Compare Evokeds Plot
![Compare Evokeds icon](images/compare/icon.png)

It can be useful to visualize multiple averages on a single plot to compare
respective signals against each other. The Compared Evokeds Plot widget
ensures this functionality.

The graphical user interface is the same as for the [EEG Plot](#eeg-plot) widget.
