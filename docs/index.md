# User Documentation
Orange3-MNE is a python package, that provides methods from MNE for Python for Orange 3 in a form of widgets, 
to allow for electrophysiological data processing.

> *Note: This library was created as a part of the master's thesis to show that it is possible to use Orange 3 as a workflow management system for electrophysiological data processing. The widgets' functionality was verified on three existing experiments. Nevertheless, the library requires further development.* 

### Installation
The installation process is quite straightforward, first we need to install the Orange 3 tool:
> *Note: If you have Orange 3 already installed, you can skip this step.*
```bash
    virtualenv orange          # Create a virtual environment
    ./orange/Scripts/activate  # Activate the environment 
    pip install Orange3 PyQt5  # Install Orange 3 and PyQt library
```
Then it is possible to install the library using one of the following methods.

##### Pip Method
```bash
   pip install Orange3-MNE
```

##### GUI Method
1. Run Orange: `python -m Orange.canvas`
2. In Orange navigate to Options -> Add-ons
3. Click on `Add more...` and enter the package name: `Orange3-MNE`
4. Confirm the settings and Orange will install the library
5. Restart Orange and the electrophysiological data processing library will be available