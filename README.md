## MNE Widgets for Orange 3 (Orange3-MNE)
[![PyPI version](https://badge.fury.io/py/Orange3-MNE.svg)](https://badge.fury.io/py/Orange3-MNE) [![Downloads](https://pepy.tech/badge/orange3-mne)](https://pepy.tech/project/orange3-mne)

Orange3-MNE is a python package, that provides methods from MNE for Python for Orange 3 in a form of widgets, 
to allow for electrophysiological data processing.

> *Note: This library was created as a part of the master's thesis to show that it is possible to use Orange 3 as a workflow management system for electrophysiological data processing. The widgets' functionality was verified on three existing experiments. Nevertheless, the library requires further development.* 

### Installation
The installation process is quite straightforward, first we need to install the Orange 3 tool:
> *Note: If you have Orange 3 already installed, you can skip this step.*
```bash
    virtualenv orange          # Create a virtual environment
    ./orange/Scripts/activate  # Activate the environment (source ./orange/Scripts/activate for linux)
    pip install Orange3 PyQt5  # Install Orange 3 and PyQt library
```

Then it is possible to install the library using one of the following methods.


##### Pip Method
```bash
   pip install Orange3-MNE
```

##### GUI Method
1. Run Orange: `python -m Orange.canvas`
2. In Orange navigate to Options -> Add-ons
3. Click on `Add more...` and enter the package name: `Orange3-MNE`
4. Confirm the settings and Orange will install the library
5. Restart Orange and the electrophysiological data processing library will be available

### User's guide
The documentation on how to use Orange 3 is available on its [homepage](https://orange.biolab.si/docs/).

The documentation to widgets in this library can be found [here](https://fifal.gitlab.io/orange-mne-library/), and [here](https://gitlab.com/fifal/orange-mne-library/-/blob/master/docs/widgets.md).

## Change Log
### 1.0.13
- Updated MNE version to 1.1.1, keras to 2.10.0, and tensorflow to 2.10.0

### 1.0.12
- Updated MNE version to 0.20.7 which resolves matplotlib problem
